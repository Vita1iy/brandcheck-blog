import express from 'express';
import mongoose from 'mongoose';
import session from 'express-session';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import bluebird from 'bluebird';
import hbs from 'express-handlebars';
import path from 'path';

import config from './www';
import mainRoute from './routes/main';
import authRoute from './routes/auth';
import userRoute from './routes/user';
import entriesRoute from './routes/entries';
import errorHandler from './middlewares/errorHandler';
import getUser from './middlewares/getUser';
import checkToken from './middlewares/checkToken';

const app = express();

mongoose.Promise = bluebird;
mongoose.connect(config.database, err => {
    if (err) throw err;
    console.log('Mongo connected');
});

/* Установка view */
app.engine('hbs', hbs({
    extname: 'hbs'
}));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, 'public')));

app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.secret
}));

app.get('/', mainRoute);
app.use('/', authRoute);
app.use('/', checkToken, userRoute);
app.use('/', checkToken, entriesRoute);

app.use(getUser);

app.use((req, res, next) => {
    res.status(404).redirect('/404');
});

app.listen(config.port, err => {
    if (err) throw err;

    console.log(`Server listen on port ${config.port}`);
});

app.use(errorHandler);