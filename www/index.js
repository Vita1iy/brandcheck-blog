const config = {
    database: 'mongodb://localhost/brandcheck',
    port: 3333,
    secret: 'brandchecksecretkey'
};

export default config;