import  express from 'express';

import * as MainController from '../controllers/main';

const router = express.Router();

router.get('/', MainController.mainPage);

export default router;