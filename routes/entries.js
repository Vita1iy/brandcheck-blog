import express from 'express';

import * as EntriesController from '../controllers/entries';

const router = express.Router();

router.post('/entries', EntriesController.create);
router.get('/entries', EntriesController.getAll);

export default router;