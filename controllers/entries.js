import Entries from '../models/Entries';

export async function create(req, res, next) {
    const entriesData = req.body;
    const userId = req.user._id;

    entriesData.userId = userId;

    try {
        var entries = await Entries.create(entriesData);
    } catch ({message}) {
        return next({
            status: 400,
            message
        });
    }

    res.json(entries);
}

export async function getAll(req, res, next) {
    try {
        var entries = await Entries.find({});
    } catch ({message}) {
        return next({
            status: 500,
            message
        });
    }

    res.json({ entries });
}