import User from '../models/Users';
import Entries from '../models/Entries';

export const mainPage = async (req, res, next) => {
    res.render('index', {
        title: 'Brandcheck'
    });
};